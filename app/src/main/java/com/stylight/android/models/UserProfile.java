package com.stylight.android.models;

public class UserProfile {

    public final String username;

    public final String password;

    public String stSecure;

    public UserProfile(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
