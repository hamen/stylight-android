package com.stylight.android;

import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.stylight.android.api.StylightApiClient;
import com.stylight.android.api.StylightService;
import com.stylight.android.managers.UserProfileManager;
import com.stylight.android.models.UserProfile;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static android.content.Context.LOCATION_SERVICE;

@Module(library = true)
public class AndroidModule {

    private final StylightApplication mApplication;

    public AndroidModule(StylightApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    @ForApplication
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    LocationManager provideLocationManager() {
        return (LocationManager) mApplication.getSystemService(LOCATION_SERVICE);
    }

    @Provides
    @Singleton
    DetailedAndroidLogger provideLogger() {
        return mApplication.getLogger();
    }
}
