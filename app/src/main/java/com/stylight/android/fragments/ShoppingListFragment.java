package com.stylight.android.fragments;

import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.stylight.android.MainActivity;
import com.stylight.android.adapters.ShoppingListAdapter;
import com.stylight.android.api.StylightApiClient;
import com.stylight.android.api.models.Product;
import com.stylight.android.api.models.ProductsResponse;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hamen.android.stylight.com.stylight.R;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ShoppingListFragment extends Fragment {

    @Inject
    StylightApiClient mStylightApiClient;

    @Inject
    DetailedAndroidLogger mLogger;

    @InjectView(R.id.grid_view)
    GridView mGridView;

    List<Product> mProducts = new ArrayList<Product>();

    @Inject
    MainActivity mActivity;

    public ShoppingListFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) getActivity()).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_list, container, false);
        ButterKnife.inject(this, view);

        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        final ShoppingListAdapter mAdapter = new ShoppingListAdapter(mActivity, mProducts);
        mGridView.setAdapter(mAdapter);

        Map<String, String> params = new HashMap<String, String>();

        params.put("request_locale", "de_DE");
        params.put("sale", "true");
        params.put("sortBy", "popularity");
        params.put("pageItems", "89");

        mActivity.setProgressBarIndeterminateVisibility(true);
        mStylightApiClient.getStylightService()
                .getProducts(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductsResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mLogger.error(e.getMessage());
                    }

                    @Override
                    public void onNext(ProductsResponse productsResponse) {
                        mActivity.setProgressBarIndeterminateVisibility(false);

                        mProducts = productsResponse.getProductlist();
                        mAdapter.setProducts(mProducts);
                    }
                });
        return view;
    }
}
