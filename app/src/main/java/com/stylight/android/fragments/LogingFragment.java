package com.stylight.android.fragments;


import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.johnpersano.supertoasts.SuperToast;
import com.nineoldandroids.animation.Animator;
import com.stylight.android.MainActivity;
import com.stylight.android.api.StylightApiClient;
import com.stylight.android.api.models.LoginResponse;
import com.stylight.android.managers.UserProfileManager;
import com.stylight.android.models.UserProfile;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hamen.android.stylight.com.stylight.R;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class LogingFragment extends Fragment {

    @Inject
    MainActivity mActivity;

    @Inject
    StylightApiClient mStylightApiClient;

    @Inject
    DetailedAndroidLogger mLogger;

    @Inject
    UserProfileManager mUserProfileManager;

    @InjectView(R.id.username)
    AutoCompleteTextView mUsernameField;

    @InjectView(R.id.password)
    EditText mPasswordField;

    @InjectView(R.id.login_button)
    Button mLoginButton;

    @InjectView(R.id.login_from)
    LinearLayout mLoginForm;

    @InjectView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private String mUsername;

    private String mPassword;

    public LogingFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) getActivity()).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.inject(this, view);

        UserProfile userprofile = mUserProfileManager.load();
        if (userprofile != null && userprofile.username != null && userprofile.password != null) {
            mLoginForm.setVisibility(View.GONE);

            mUsername = userprofile.username;
            mPassword = userprofile.password;

            login(mUsername, mPassword);
        }

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mUsername = String.valueOf(mUsernameField.getText());
                mPassword = String.valueOf(mPasswordField.getText());
                if ("".equals(mUsername) && "".equals(mPassword)) {
                    // Login with my credentials
                    mUsername = "hamen_stylight";
                    mPassword = "hamen12345";
                }

                YoYo.with(Techniques.SlideOutDown)
                        .duration(1000)
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                YoYo.with(Techniques.SlideInDown).duration(1000).playOn(mProgressBar);
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        })
                        .playOn(mLoginForm);

                login(mUsername, mPassword);
            }
        });
        return view;
    }

    private void login(String username, String password) {

        mStylightApiClient.getStylightService()
                .login(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(new Func1<LoginResponse, Boolean>() {
                    @Override
                    public Boolean call(LoginResponse loginResponse) {
                        return "SUCCESS".equals(loginResponse.getStatus());
                    }
                })
                .subscribe(new Observer<LoginResponse>() {
                    @Override
                    public void onCompleted() {
                        mLogger.info("Login successfull");
                    }

                    @Override
                    public void onError(Throwable e) {
                        mLogger.error(e.getMessage());

                        SuperToast superToast = new SuperToast(mActivity);
                        superToast.setDuration(SuperToast.Duration.LONG);
                        superToast.setText(mActivity.getString(R.string.login_error_message));
                        superToast.setIcon(SuperToast.Icon.Dark.INFO, SuperToast.IconPosition.LEFT);
                        superToast.show();

                        YoYo.with(Techniques.SlideInUp).duration(1000).playOn(mLoginForm);

                        mUserProfileManager.delete();
                    }

                    @Override
                    public void onNext(LoginResponse loginResponse) {

                        UserProfile userProfile = new UserProfile(mUsername, mPassword);
                        mUserProfileManager.store(userProfile);

                        mActivity.getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, new ShoppingListFragment())
                                .commit();
                    }
                });
    }
}
