package com.stylight.android.api;

import com.stylight.android.managers.UserProfileManager;
import com.stylight.android.models.UserProfile;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.protocol.BasicHttpContext;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.experimental.Accessors;
import retrofit.RestAdapter;
import retrofit.client.ApacheClient;
import retrofit.client.Client;

@Singleton
@Accessors(prefix = "m")
public class StylightApiClient {

    UserProfileManager mUserProfileManager;

    @Getter
    private final StylightService mStylightService;

    private final ApiHeaders mApiHeaders =  new ApiHeaders();

    @Inject
    public StylightApiClient(UserProfileManager userProfileManager) {
        mUserProfileManager = userProfileManager;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint("http://api.stylight.com")
                .setRequestInterceptor(mApiHeaders)
                .setClient(mApacheClient)
                .build();

        mStylightService = restAdapter.create(StylightService.class);
    }

    Client mApacheClient = new ApacheClient() {
        final CookieStore cookieStore = new BasicCookieStore();

        @Override
        protected HttpResponse execute(HttpClient client, HttpUriRequest request) throws IOException {

            BasicHttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

            for (Cookie cookie : cookieStore.getCookies()) {
                if ("st_secure".equals(cookie.getName())) {
                    String stSecure = cookie.getValue();
                    mApiHeaders.setStSecure("st_secure=" + stSecure);

                    UserProfile userprofile = mUserProfileManager.load();
                    userprofile.stSecure = stSecure;
                    mUserProfileManager.store(userprofile);

                }
                if ("JSESSIONID".equals(cookie.getName())) {
                    mApiHeaders.setSessionId(cookie.getValue());
                }
            }
            return client.execute(request, httpContext);
        }
    };
}
