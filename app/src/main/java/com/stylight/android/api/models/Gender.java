package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import lombok.Data;

@Data
public class Gender {

    @Expose
    private int genid;

    @Expose
    private String genname;
}
