package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import lombok.Data;

@Data
public class LoginResponse {

    @Expose
    private String build;

    @Expose
    private String loginstatus;

    @Expose
    private String status;

    @Expose
    private String timestamp;
}