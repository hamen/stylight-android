package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import lombok.Data;

@Data
public class Currency {

    @Expose
    private int curid;

    @Expose
    private String curname;
}
