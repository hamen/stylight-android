package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import lombok.Data;

@Data
public class Shop {

    @Expose
    private int id;

    @Expose
    private String name;

    @Expose
    private boolean premium;

    @Expose
    private Site site;

}
