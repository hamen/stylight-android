package com.stylight.android.api;

import com.stylight.android.api.models.LoginResponse;
import com.stylight.android.api.models.ProductsResponse;

import java.util.Map;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.QueryMap;
import rx.Observable;

public interface StylightService {

    @FormUrlEncoded
    @Headers({"X-apiKey: D13A5A5A0A3602477A513E02691A8458", "X-Recruiting: If you are reading this, you should hire me."})
    @POST("/api/login")
    Observable<LoginResponse> login(@Field("username") String username, @Field("passwd") String password);


    @GET("/api/products")
    @Headers({"X-apiKey: D13A5A5A0A3602477A513E02691A8458", "Accept­Language: de-DE", "X-Recruiting: If you are reading this, you should hire me."})
    Observable<ProductsResponse> getProducts(@QueryMap Map<String, String> options);
}
