package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Product {

    @Expose
    private boolean available;

    @Expose
    private Brand brand;

    @Expose
    private Currency currency;

    @Expose
    private String date;

    @Expose
    private String desc;

    @Expose
    private Gender gender;

    @Expose
    private int id;

    @Expose
    private List<Image> images = new ArrayList<Image>();

    @Expose
    private boolean liked;

    @Expose
    private int likes;

    @Expose
    private int masterProductId;

    @Expose
    private String name;

    @Expose
    private float price;

    @Expose
    private boolean sale;

    @Expose
    private float savings;

    @Expose
    private float shippingCost;

    @Expose
    private Shop shop;

    @Expose
    private String shopLink;

    @Expose
    private String url;
}
