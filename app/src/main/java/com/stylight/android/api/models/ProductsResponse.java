package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ProductsResponse {

    @Expose
    private String build;

    @Expose
    private int count;

    @Expose
    private List<Product> productlist = new ArrayList<Product>();

    @Expose
    private int start;

    @Expose
    private Statistics statistics;

    @Expose
    private String status;

    @Expose
    private String timestamp;

    @Expose
    private int totalresults;
}
