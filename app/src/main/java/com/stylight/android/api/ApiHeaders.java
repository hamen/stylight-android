package com.stylight.android.api;

import lombok.Setter;
import lombok.experimental.Accessors;
import retrofit.RequestInterceptor;

@Accessors(prefix = "m")
public class ApiHeaders implements RequestInterceptor {

        @Setter
        private String mSessionId;

        @Setter
        private String mStSecure;

        public void clearSessionId() {
            mSessionId = null;
        }

        @Override
        public void intercept(RequestFacade request) {
            if (mSessionId != null) {
                request.addHeader("Cookie", mStSecure);
            }
        }
    }