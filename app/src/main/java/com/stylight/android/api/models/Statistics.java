package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Statistics {

    @Expose
    private List<Object> brandStatistics = new ArrayList<Object>();

    @Expose
    private List<Object> freeShippingStatistics = new ArrayList<Object>();

    @Expose
    private List<Object> saleStatistics = new ArrayList<Object>();

    @Expose
    private List<Object> shopStatistics = new ArrayList<Object>();

    @Expose
    private List<Object> tagGroupStatistics = new ArrayList<Object>();

    @Expose
    private List<Object> tagStatistics = new ArrayList<Object>();
}
