package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import lombok.Data;

@Data
public class Image {

    @Expose
    private boolean primary;

    @Expose
    private String url;
}
