package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import lombok.Data;

@Data
public class Brand {

    @Expose
    private int bid;

    @Expose
    private String bname;
}
