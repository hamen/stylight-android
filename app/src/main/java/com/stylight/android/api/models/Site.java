package com.stylight.android.api.models;

import com.google.gson.annotations.Expose;

import lombok.Data;

@Data
public class Site {

    @Expose
    private int id;

    @Expose
    private String locale;
}
