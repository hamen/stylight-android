package com.stylight.android.adapters;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.stylight.android.MainActivity;
import com.stylight.android.api.models.Product;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hamen.android.stylight.com.stylight.R;

public class ShoppingListAdapter extends BaseAdapter {

    MainActivity mActivity;

    private final LayoutInflater mInflater;

    private List<Product> mProducts;

    public ShoppingListAdapter(MainActivity activity, List<Product> products) {
        mActivity = activity;
        mInflater = activity.getLayoutInflater();
        mProducts = products;
    }

    @Override
    public int getCount() {
        return mProducts.size();
    }

    @Override
    public Product getItem(int position) {
        return mProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.shopping_list_item, parent, false);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Product product = getItem(position);

        holder.brand.setText(product.getBrand().getBname());
        holder.name.setText(product.getName());

        if (product.isSale()) {
            holder.not_in_sale.setVisibility(View.GONE);

            DecimalFormat f = new DecimalFormat("####.00");
            holder.price.setText(String.valueOf(f.format(product.getPrice() * product.getSavings())) + "€");
            holder.price.setTextColor(mActivity.getResources().getColor(R.color.sale_price_text));

            holder.price2.setText(String.valueOf(product.getPrice()) + "€");
            holder.price2.setPaintFlags(holder.price2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.not_in_sale.bringToFront();
            holder.price.setText(String.valueOf(product.getPrice()) + "€");
            holder.price2.setText("");
        }

        ImageLoader.getInstance().displayImage(product.getImages().get(0).getUrl(), holder.image);

        return convertView;
    }

    public void setProducts(List<Product> products) {
        mProducts.clear();
        mProducts.addAll(products);
        notifyDataSetChanged();
    }

    static class ViewHolder {

        @InjectView(R.id.brand)
        TextView brand;

        @InjectView(R.id.name)
        TextView name;

        @InjectView(R.id.price)
        TextView price;

        @InjectView(R.id.price2)
        TextView price2;

        @InjectView(R.id.image)
        ImageView image;

        @InjectView(R.id.not_in_sale)
        View not_in_sale;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
