package com.stylight.android;

import com.stylight.android.api.StylightApiClient;
import com.stylight.android.fragments.LogingFragment;
import com.stylight.android.fragments.ShoppingListFragment;
import com.stylight.android.managers.UserProfileManager;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        injects = {
                MainActivity.class,
                LogingFragment.class,
                ShoppingListFragment.class,
                UserProfileManager.class,
                StylightApiClient.class
        },
        addsTo = AndroidModule.class,
        library = true
)
public class ActivityModule {

    private final MainActivity mActivity;

    public ActivityModule(MainActivity activity) {
        mActivity = activity;
    }

    @Provides
    @Singleton
    @ForActivity
    Context provideActivityContext() {
        return mActivity;
    }

    @Provides
    @Singleton
    MainActivity provideActivity() {
        return mActivity;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharePreferences() {
        return mActivity.getPreferences(Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    StylightApiClient provideStylightClient(UserProfileManager userProfileManager) {
        return new StylightApiClient(userProfileManager);
    }
}
