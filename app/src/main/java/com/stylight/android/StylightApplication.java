package com.stylight.android;

import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.alterego.advancedandroidlogger.interfaces.IAndroidLogger;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.stylight.android.api.StylightApiClient;
import com.stylight.android.api.StylightService;

import android.app.Application;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;
import hamen.android.stylight.com.stylight.R;
import lombok.Getter;
import lombok.experimental.Accessors;

@Accessors(prefix = "m")
public class StylightApplication extends Application {

    @Getter
    private ObjectGraph mApplicationGraph;

    @Getter
    private DetailedAndroidLogger mLogger;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationGraph = ObjectGraph.create(getModules().toArray());

        mLogger = new DetailedAndroidLogger("STYLIGHT", IAndroidLogger.LoggingLevel.VERBOSE);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.error_placeholder)
                .showImageOnLoading(R.drawable.ic_launcher)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
    }

    /**
     * A list of modules to use for the application graph. Subclasses can override this method to provide additional modules provided they call {@code
     * super.getModules()}.
     */
    protected List<Object> getModules() {
        return Arrays.<Object>asList(new AndroidModule(this));
    }
}
