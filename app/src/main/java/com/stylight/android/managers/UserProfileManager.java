package com.stylight.android.managers;

import com.google.gson.Gson;

import com.stylight.android.MainActivity;
import com.stylight.android.models.UserProfile;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.functions.Action0;
import rx.schedulers.Schedulers;

@Singleton
public class UserProfileManager {

    SharedPreferences mSharedPreferences;

    @Inject
    public UserProfileManager(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public void store(final UserProfile userProfile) {
        Schedulers.io().createWorker().schedule(new Action0() {
            @Override
            public void call() {
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putString("profile", new Gson().toJson(userProfile));
                editor.commit();
            }
        });
    }

    public UserProfile load() {
        String profile = mSharedPreferences.getString("profile", "");
        return new Gson().fromJson(profile, UserProfile.class);
    }

    public void delete() {
        mSharedPreferences.edit().remove("profile").apply();
    }
}
