## General questions

* Usually I contribute to libraries. I only work on my own apps.

* In my former company, we had to create a streaming video player with realtime remote data overlays. Android is a really _poor_ framework when it comes to event driven, asyncronous, concurrent programming. That's why I started using [RxJava](https://github.com/Netflix/RxJava). RxJava is an extraordinary tools, that allows me to create well structured apps, with reactive UI, non-blocking, asyncronous data retrieving etc.

## Skill test

* I would definitely go with an ORM. I have experience with GreenDAO, but ActiveAndroid will do good too.
Every fragment/activity reads the products list from the database table and subscribe to a table observable. When it comes to update/delete elements in the list, we just update the table and emit an onNext() on the table observer.

## The App
The app has been tested on Nexus5 (Android L) and Nexus10 (Android 4.4.4).
  
  + It's Gradle based, GIT + GIT FLOW versioned
  + I used fragments, of course
  + Dagger as dependency injection system
  + ButterKnife as View Injector
  + Lombok to save a bit of Java Boilerplate
  + Gson and JodaTime to handle the API JSON response
  + RxJava as Reactive Programming framework
  + Retrofit and okhttp as a REST API client 
  + SuperToast and Android View Animations to add some fancy UXD
  + Univeral Image Loader as image downloading/caching system
  + Advanved Android Logger as logging system